## code to prepare `pays` dataset goes here

pays <- read.csv("data-raw/pays.csv", header = FALSE, row.names=1)
colnames(pays) <- c("No", "Abreviation2", 'Abreviation3', "NomFr", "NomEn")
Encoding(pays$NomFr) <- "UTF-8"
Encoding(pays$NomEn) <- "UTF-8"

usethis::use_data(pays, overwrite = TRUE)
